import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import amplitude from "./../../../utils/amplitude";

function LoginButton() {
  const { loginWithRedirect } = useAuth0();

  return (
    <div>
      <button
        className="button is-info"
        onClick={() => {
          loginWithRedirect();
          amplitude.logEvent("LoginButtonClicked");
        }}
      >
        Log In
      </button>
    </div>
  );
}

export default LoginButton;
