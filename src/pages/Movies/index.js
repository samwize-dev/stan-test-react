import React, { useState, useEffect } from 'react';
import Banner from './components/Banner';
import Utils from '../../utils/DemoUtils';

export default function Movies(props) {
  const [movies, setMovies] = useState([]);
  const [error, setError] = useState('');

  const getMovies = async () => {
    await fetch('/feeds/sample.json')
      .then((response) => response.json())
      .then((data) => {
        const moviesData = data.entries
          .filter(
            (data) => data.programType === 'movie' && data.releaseYear >= 2010
          )
          .slice(0, 21)
          .sort(Utils.compare);

        setMovies(moviesData);
      })
      .catch((err) => {
        setError('Oops! Something went wrong');
      });
  };

  useEffect(() => {
    getMovies();
  }, []);

  return (
    <div className='root'>
      <Banner />

      <div className='container mx-auto'>
        <section className='section'>
          {movies.length ? (
            movies.map((item, i) => (
              <div key={i} className='section-item'>
                <div
                  className='card'
                  style={{
                    backgroundImage: `url(${item.images['Poster Art'].url})`,
                  }}
                ></div>
                <div className='card-item'>
                  <a href={item.url}>
                    <p>{item.title}</p>
                  </a>
                </div>
              </div>
            ))
          ) : (
            <div>{error ? <p>{error}</p> : <p>Loading...</p>}</div>
          )}
        </section>
      </div>
    </div>
  );
}
