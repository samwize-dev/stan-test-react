import React, { lazy, Suspense } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Layout from './components/layout';
import Home from './pages/Home/';
import Auth0Provider from './pages/Auth/auth-provider';
import PrivateRoute from './utils/privateRoute';

import './utils/chargebee';

const Movies = lazy(() => import('./pages/Movies'));
const Series = lazy(() => import('./pages/Series'));
const Login = lazy(() => import('./pages/Auth'));
const Profile = lazy(() => import('./pages/Profile'));
const Player = lazy(() => import('./pages/Player'));

function App() {
  return (
    <div className='App'>
      <Router>
        <Auth0Provider>
          <Suspense fallback={<div>Loading...</div>}>
            <Switch>
              <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/movies' component={Movies} />
                <Route path='/series' component={Series} />
                <Route path='/login' component={Login} />
                <Route path='/player' component={Player} />
                <PrivateRoute path='/profile' component={Profile} />
                <Route path='*' component={Home} />
              </Layout>
            </Switch>
          </Suspense>
        </Auth0Provider>
      </Router>
    </div>
  );
}

export default App;
