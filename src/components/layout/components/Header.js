import React from "react";
import amplitude from "./../../../utils/amplitude";

export default function Header() {
  return (
    <header className="header">
      <div className="container mx-auto">
        <nav className="nav">
          <div className="logo">
            <img src="" alt="Logo" />
          </div>
          <div className="col-1">
            <a href="/" className="title">
              <h2>Demo Streaming</h2>
            </a>
          </div>
          <div className="col-2">
            <a
              href="/login"
              className="button"
              onClick={() => amplitude.logEvent("loginButtonClicked")}
            >
              Log in
            </a>
            <a
              href="/profile"
              className="button"
              onClick={() => amplitude.logEvent("profileButtonClicked")}
            >
              Profile
            </a>
            <a
              href="/"
              className="button trialBtn"
              onClick={() => amplitude.logEvent("trialEventClicked")}
            >
              Start your free trial
            </a>
          </div>
        </nav>
      </div>
    </header>
  );
}
