import React from 'react';
import Plyr from 'plyr-react';
import 'plyr-react/dist/plyr.css';

function VideoPlyrReact() {
  return (
    <div className='container my-6'>
      <Plyr
        source={{
          type: 'video',
          poster:
            'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg',
          tracks: [
            {
              src:
                'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt',
              default: true,
            },
          ],
          sources: [
            {
              src:
                'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4',
              type: 'video/mp4',
              size: 576,
            },
          ],
        }}
        options={{
          settings: ['captions', 'quality', 'speed'],
          speed: {
            selected: 1,
            options: [1, 1.25, 1.5, 2],
          },
          captions: {
            active: false,
            language: 'auto',
            // Listen to new tracks added after Plyr is initialized.
            // This is needed for streaming captions, but may result in unselectable options
            update: false,
          },
          previewThumbnails: {
            enabled: true,
            src:
              'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt',
          },
          seekTime: 5,
          tooltips: { controls: true, seek: true },
          loadSprite: true,
          iconPrefix: 'plyr',
          iconUrl: 'https://cdn.plyr.io/3.5.2/plyr.svg',
          controls: [
            'rewind',
            'play',
            'play-large',
            'fast-forward',
            'progress',
            'current-time',
            'duration',
            'mute',
            'volume',
            'settings',
            'fullscreen',
            'pip',
            'airplay',
          ],
        }}
      />
    </div>
  );
}

export default VideoPlyrReact;
