import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import LoginButton from "./components/LoginButton";
import LogoutButton from "./components/LogoutButton";

function Authenticated() {
  const { isAuthenticated } = useAuth0();

  return (
    <div className="container my-6">
      <div className="columns is-mobile is-8">
        <div
          className="column is-half is-offset-one-quarter"
          style={{ border: "2px solid #dcdcdc" }}
        >
          {isAuthenticated ? <LogoutButton /> : <LoginButton />}
        </div>
      </div>
    </div>
  );
}

export default Authenticated;
