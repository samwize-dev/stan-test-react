import React from "react";

export default function Banner() {
  return (
    <div className="banner">
      <div className="container mx-auto">
        <div className="w-full">
          <h3>Popular Title</h3>
        </div>
      </div>
    </div>
  );
}
