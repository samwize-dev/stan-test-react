import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

function Profile() {
  const { user } = useAuth0();

  return (
    <div className="container my-6">
      <div className="columns is-mobile is-8">
        <div
          className="column is-half is-offset-one-quarter"
          style={{ border: "2px solid #dcdcdc" }}
        >
          {JSON.stringify(user, null, 2)}
        </div>
      </div>
    </div>
  );
}

export default Profile;
