import React from "react";

export default function Footer() {
  return (
    <footer className="footer">
      <div className="container mx-auto">
        <nav>
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/">Terms & Conditions</a>
            </li>
            <li>
              <a href="/">Privacy Policy</a>
            </li>
            <li>
              <a href="/">Collection Statement</a>
            </li>
            <li>
              <a href="/">Help</a>
            </li>
            <li>
              <a href="/">Manage Accounts</a>
            </li>
          </ul>
        </nav>
        <p className="copyright">
          Copyright &copy; {new Date().getFullYear()} DEMO Streaming. All Rights
          Reserved.
        </p>

        <div className="socials">
          <div className="social-icons">
            <a href="https://www.facebook.com/okekesamwize">
              <img src="assets/social/facebook-white.svg" alt="" />
            </a>
            <a href="https://www.twitter.com/sampedia">
              <img src="assets/social/twitter-white.svg" alt="" />
            </a>
            <a href="https://www.instagram.com/samwize.o/">
              <img src="assets/social/instagram-white.svg" alt="" />
            </a>
          </div>

          <div className="store-icons">
            <img src="assets/store/app-store.svg" alt="" />
            <img src="assets/store/play-store.svg" alt="" />
            <img src="assets/store/windows-store.svg" alt="" />
          </div>
        </div>
      </div>
    </footer>
  );
}
