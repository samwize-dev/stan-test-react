import styled from 'styled-components';

const Section = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 300px);
  gap: 1rem;
  place-content: center;
  padding: 2rem;
  @media screen and (max-width: 768px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;

const SectionItem = styled.div`
  color: #fff;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
  align-items: center;
  text-align: center;
  border: 1px solid #efefef;
`;

const Card = styled.div`
  background-color: #000;
  background-image: url(/assets/placeholder.png);
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1 1 300px;
  width: 100%;
  padding: 0.2rem 0.5rem;
  border: 3px solid green;
`;

const CardTile = styled.div`
  color: #000;
  padding: 1rem;
`;

export { Section, SectionItem, Card, CardTile };
