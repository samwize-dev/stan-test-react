import React, { useState, useEffect } from 'react';
import Banner from './components/Banner';
import Utils from '../../utils/DemoUtils';

export default function Series(props) {
  const [series, setSeries] = useState([]);
  const [error, setError] = useState('');

  const getSeries = async () => {
    await fetch('/feeds/sample.json')
      .then((response) => response.json())
      .then((data) => {
        const moviesData = data.entries
          .filter(
            (data) => data.programType === 'series' && data.releaseYear >= 2010
          )
          .slice(0, 21)
          .sort(Utils.compare);

        setSeries(moviesData);
      })
      .catch((err) => {
        setError('Oops! Something went wrong');
      });
  };

  useEffect(() => {
    getSeries();
  }, []);

  return (
    <div className='root'>
      <Banner />

      <div className='container mx-auto'>
        <section className='section'>
          {series.length ? (
            series.map((item, i) => (
              <div key={i} className='section-item'>
                <div
                  className='card'
                  style={{
                    backgroundImage: `url(${item.images['Poster Art'].url})`,
                  }}
                ></div>
                <div className='card-item'>
                  <a href={item.url}>
                    <p>{item.title}</p>
                  </a>
                </div>
              </div>
            ))
          ) : (
            <div>{error ? <p>{error}</p> : <p>Loading...</p>}</div>
          )}
        </section>
      </div>
    </div>
  );
}
